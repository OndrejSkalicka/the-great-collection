#!/bin/bash

echo "Git pull"

git pull

echo "Backing up DB"

docker-compose -f docker-compose-rpi4.yml exec db-backup /backup.sh deploy_backup

echo "DB backed up, rebuilding images and redeploying."

docker-compose -f docker-compose-rpi4.yml build && docker-compose -f docker-compose-rpi4.yml down && docker-compose -f docker-compose-rpi4.yml up -d && docker ps

echo "All done"
