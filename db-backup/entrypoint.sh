#!/bin/sh

trap exit TERM

#while :; do sleep 24h & wait $${!}; /backup.sh; done;

echo "$(date '+%Y-%m-%d %H:%M:%S') Starting backup loop."

while :
do
  echo "$(date '+%Y-%m-%d %H:%M:%S') Backing up..."

  export PGPASSWORD="$POSTGRES_PASSWORD"
  pg_dump -c --if-exists -h postgres -U $POSTGRES_USER -w -d $POSTGRES_DB | gzip > /pg_backup/${1:-dump}_`date +%Y-%m-%d_%H-%M-%S_%Z`.sql.gz

  # restore via:
  #
  # export PGPASSWORD="$POSTGRES_PASSWORD" && zcat /pg_backup/dump_XXX.sql.gz | psql -h postgres -d $POSTGRES_DB -U $POSTGRES_USER

  echo "$(date '+%Y-%m-%d %H:%M:%S') Backed up, sleeping."
  sleep 24h & wait ${!}
done
