import django
import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration

from tgcweb.settings.base import *

# DEBUG
# ------------------------------------------------------------------------------
DEBUG = False
ALLOWED_HOSTS = env.list('DJANGO_ALLOWED_HOSTS', default=['magic-collection.eu', '*'])

if 'django.middleware.security.SecurityMiddleware' in MIDDLEWARE:
    MIDDLEWARE.insert(MIDDLEWARE.index('django.middleware.security.SecurityMiddleware') + 1,
                      'whitenoise.middleware.WhiteNoiseMiddleware')
else:
    MIDDLEWARE.insert(0, 'whitenoise.middleware.WhiteNoiseMiddleware')

STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'


def before_send(event, hint):
    if "log_record" in hint and hint["log_record"].name == "django.security.DisallowedHost":
        return None
    return event


def traces_sampler(sampling_context):
    if 'wsgi_environ' in sampling_context and 'PATH_INFO' in sampling_context['wsgi_environ'] and sampling_context['wsgi_environ']['PATH_INFO'] == '/':
        return 0.0
    return 0.1


sentry_sdk.init(
    dsn=env('SENTRY_DSN'),
    integrations=[DjangoIntegration()],

    traces_sampler=traces_sampler,

    # If you wish to associate users to errors (assuming you are using
    # django.contrib.auth) you may enable sending PII data.
    send_default_pii=True,

    before_send=before_send,
)
