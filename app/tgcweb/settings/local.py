from tgcweb.settings.base import *

# DEBUG
# ------------------------------------------------------------------------------
DEBUG = env.bool('DJANGO_DEBUG', default=True)
INTERNAL_IPS = ['127.0.0.1', ]
ALLOWED_HOSTS = ['*']

TEMPLATES[0]['OPTIONS']['debug'] = DEBUG

INSTALLED_APPS += ['debug_toolbar', ]
MIDDLEWARE += ['debug_toolbar.middleware.DebugToolbarMiddleware', ]

ACCOUNT_DEFAULT_HTTP_PROTOCOL = 'http'
SECRET_KEY = env('DJANGO_SECRET_KEY', default='&li_id-2l!iam-pkbc=xr7&_(*uf5rz1d5m@!uvdjvs75o@!4=')

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
