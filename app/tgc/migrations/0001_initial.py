# Generated by Django 3.2.6 on 2021-08-15 22:47

import django.db.models.deletion
from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Card',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('en_name', models.CharField(max_length=128)),
                ('oracle_id', models.UUIDField()),
                ('mana_cost', models.CharField(max_length=32)),
                ('cmc', models.FloatField()),
                ('type', models.CharField(max_length=128)),
                ('card_json', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='SetEdition',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('scryfall_id', models.UUIDField()),
                ('code', models.CharField(max_length=32)),
                ('name', models.CharField(max_length=32)),
                ('released_at', models.CharField(max_length=10)),
                ('icon_svg_uri', models.URLField()),
            ],
        ),
        migrations.CreateModel(
            name='Collection',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=128)),
                ('slug', models.SlugField(editable=False, max_length=128)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='CardPrint',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('released_at', models.CharField(max_length=10)),
                ('scryfall_id', models.UUIDField(unique=True)),
                ('card_json', models.TextField()),
                ('image_uri_large', models.URLField(null=True)),
                ('image_uri_normal', models.URLField(null=True)),
                ('image_uri_small', models.URLField(null=True)),
                ('image_uri_art_crop', models.URLField(null=True)),
                ('card', models.ForeignKey(on_delete=django.db.models.deletion.RESTRICT, to='tgc.card')),
                ('set_edition', models.ForeignKey(on_delete=django.db.models.deletion.RESTRICT, to='tgc.setedition')),
            ],
        ),
        migrations.CreateModel(
            name='CardInCollection',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('count', models.PositiveSmallIntegerField()),
                ('foil', models.BooleanField(default=False)),
                ('card_print', models.ForeignKey(on_delete=django.db.models.deletion.RESTRICT, to='tgc.cardprint')),
                ('collection', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='tgc.collection')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
