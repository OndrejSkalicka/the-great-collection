from django.contrib import admin

from tgc.models import Collection, UserProfile

admin.site.register(Collection)
admin.site.register(UserProfile)
