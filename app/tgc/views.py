import csv
import io
import json

import xlsxwriter
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.db.models import Sum
from django.http import JsonResponse, HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from django.views.decorators.http import require_POST, require_GET

from tgc.models import Collection, CardInCollection
from tgc.scryfall import get_or_create_card_print


def index(request):
    if request.user.is_authenticated:
        return redirect(reverse('tgc:user_home', kwargs={
            'user_id': request.user.id,
            'public_key': request.user.userprofile.public_link_key,
        }))

    return render(request, 'tgc/index.html')


@login_required
def add_cards(request):
    # TODO complete cleanup / refactoring of this file
    return render(request, 'tgc/add_cards.html', {
        'collections': Collection.objects.filter(user=request.user).all()
    })


@login_required
def add_cards_to_collection(request, collection_id):
    return render(request, 'tgc/add_cards.html', {
        'collections': Collection.objects.filter(user=request.user).all(),
        'selected_collection_id': collection_id,
    })


def legacy_redirect_cards(request, user_id: int, public_key: str):
    user = get_object_or_404(User, id=user_id, userprofile__public_link_key=public_key)

    return redirect(reverse('tgc:user_all_cards', kwargs={
        'user_id': user.id,
        'public_key': user.userprofile.public_link_key,
    }))


def user_home(request, user_id: int, public_key: str):
    user = get_object_or_404(User, id=user_id, userprofile__public_link_key=public_key)

    collection_counts = {
        csum['collection_id']: csum['count__sum'] for csum in
        CardInCollection.objects.filter(collection__user=user).values('collection_id').annotate(Sum('count'))
    }

    collections = list(
        (c, collection_counts.get(c.id, 0)) for c in
        Collection.objects.filter(user=user).select_related('user__userprofile').all()
    )

    return render(request, 'tgc/user_home.html', {
        'owner': user,
        'is_owner': user == request.user,
        'collections': collections,
    })


def user_all_cards(request, user_id: int, public_key: str):
    user = get_object_or_404(User, id=user_id, userprofile__public_link_key=public_key)
    is_owner = user == request.user
    collections = []
    if is_owner:
        collections = Collection.objects.filter(user=user.id).all()

    return render(request, 'tgc/cards.html', {
        'owner': user,
        'is_owner': is_owner,
        'collections': collections,
    })


def user_collections(request, user_id: int, public_key: str):
    user = get_object_or_404(User, id=user_id, userprofile__public_link_key=public_key)

    collection_counts = {
        csum['collection_id']: csum['count__sum'] for csum in
        CardInCollection.objects.filter(collection__user=user).values('collection_id').annotate(Sum('count'))
    }

    collections = list(
        (c, collection_counts.get(c.id, 0)) for c in
        Collection.objects.filter(user=user).select_related('user__userprofile').all()
    )

    return render(request, 'tgc/collections.html', {
        'owner': user,
        'is_owner': user == request.user,
        'collections': collections,
    })


def user_collection_detail(request, user_id: int, public_key: str, collection_id: int):
    user = get_object_or_404(User, id=user_id, userprofile__public_link_key=public_key)
    collection = get_object_or_404(Collection, user=user, id=collection_id)
    is_owner = user == request.user
    collections = []
    if is_owner:
        collections = Collection.objects.filter(user=user.id).all()

    return render(request, 'tgc/cards.html', {
        'collection': collection,
        'owner': user,
        'is_owner': is_owner,
        'collections': collections,
    })


@login_required
@require_POST
def create_collection(request):
    collection = Collection(
        name=request.POST['name'],
        note=request.POST['note'],
        type=request.POST['type'],
        user=request.user
    )
    collection.save()

    messages.add_message(request, messages.SUCCESS, 'Binder / Deck %s created.' % collection.name)
    return redirect(request.POST['next'])


@login_required
@require_POST
def edit_collection(request):
    collection = get_object_or_404(Collection, user=request.user, id=request.POST['id'])

    collection.name = request.POST['name']
    collection.note = request.POST['note']
    collection.type = request.POST['type']
    collection.save()

    messages.add_message(request, messages.SUCCESS, 'Binder / Deck %s updated.' % collection.name)
    return redirect(request.POST['next'])


@login_required
@require_POST
def delete_collection(request):
    collection = get_object_or_404(Collection, user=request.user, id=request.POST['id'])

    if len(collection.cardincollection_set.all()) > 0:
        messages.add_message(request, messages.ERROR,
                             'Binder / Deck %s has cards in it, cannot be deleted.' % collection.name)

    else:
        collection.delete()
        messages.add_message(request, messages.SUCCESS, 'Binder / Deck %s deleted.' % collection.name)

    return redirect(request.POST['next'])


@login_required
@require_POST
def move_cards_from_to_collection(request):
    collection = get_object_or_404(Collection, user=request.user, id=request.POST['collection-id'])

    moved_ids = list(x[0][len('card_move_'):] for x in request.POST.items() if x[0].startswith('card_move_'))

    for id in moved_ids:
        cic = CardInCollection.objects.select_related('collection').filter(collection__user=request.user, id=id).get()
        count = int(request.POST['card_count_%s' % id])

        move_single_card_to_collection(cic, count, collection)

    messages.add_message(request, messages.SUCCESS, 'Cards moved.')

    return redirect(request.POST['next'])


@login_required
@require_POST
def move_card_to_collection(request):
    collection = get_object_or_404(Collection, user=request.user, id=request.POST['collection-id'])

    cic = CardInCollection.objects.select_related('collection').filter(collection__user=request.user,
                                                                       id=request.POST['id']).get()
    count = int(request.POST['count'])

    move_single_card_to_collection(cic, count, collection)

    messages.add_message(request, messages.SUCCESS, 'Cards moved.')

    return redirect(request.POST['next'])


@login_required
@require_POST
def edit_card(request):
    cic = get_object_or_404(CardInCollection, collection__user=request.user, id=request.POST['id'])

    cic.count = int(request.POST['count'])
    cic.note = request.POST.get('note')

    cic.save()

    messages.add_message(request, messages.SUCCESS, 'Card %s updated, count %dx, note "%s".' % (
        cic.card_print.card.name, cic.count, cic.note
    ))

    return redirect(request.POST['next'])


def export_cards_xlsx(request, user_id: int, public_key: str):
    qs = CardInCollection.objects.filter(collection__user_id=user_id,
                                         collection__user__userprofile__public_link_key=public_key) \
        .select_related('collection', 'card_print', 'card_print__card', 'card_print__set_edition') \
        .order_by('card_print__card__name', 'card_print__released_at', 'collection_id').all()

    if request.GET.get('format') == 'dragonshield':
        output = io.BytesIO()
        wrapper = io.TextIOWrapper(output, encoding='utf-8', line_buffering=True)

        writer = csv.writer(wrapper, delimiter=',', quoting=csv.QUOTE_MINIMAL)

        writer.writerow([
            'Folder Name', 'Quantity', 'Trade Quantity', 'Card Name', 'Set Code', 'Set Name',
            'Card Number', 'Condition', 'Printing', 'Language', 'Price Bought', 'Date Bought'
        ])
        for cic in qs:
            card_json = json.loads(cic.card_print.card_json)
            card_lang_name = {
                'en': 'English',
                'es': 'Spanish',
                'fr': 'French',
                'de': 'German',
                'it': 'Italian',
                'pt': 'Portuguese',
                'ja': 'Japanese',
                'ko': 'Korean',
                'ru': 'Russian',
                'zhs': 'Simplified Chinese',
                'zht': 'Traditional Chinese',
                'he': 'Hebrew',
                'la': 'Latin',
                'grc': 'Ancient Greek',
                'ar': 'Arabic',
                'sa': 'Sanskrit',
                'ph': 'Phyrexian',
            }[cic.card_print.lang]

            writer.writerow([
                cic.collection.name,
                cic.count,
                0,
                cic.card_print.card.name,
                cic.card_print.set_edition.code,
                cic.card_print.set_edition.name,
                card_json.get('collector_number'),
                'Good',
                'Foil' if cic.foil else 'Normal',
                card_lang_name,
                '0.0',
                cic.created_at.strftime('%m/%d/%Y'),
            ])

        output.seek(0)

        filename = 'magic-collection-export.csv'
        response = HttpResponse(
            output,
            content_type='text/csv'
        )
        response['Content-Disposition'] = 'attachment; filename=%s' % filename

    else:

        output = io.BytesIO()
        workbook = xlsxwriter.Workbook(output)
        worksheet = workbook.add_worksheet()

        header = ['Count', 'Card', 'Lang', 'Localized Name', 'Type', 'P/T', 'Cost', 'Set', 'Note', 'Binder / Deck']
        for col_num, col in enumerate(header):
            worksheet.write(0, col_num, col)

        row_num = 1
        for cic in qs:
            worksheet.write(row_num, 0, cic.count)
            worksheet.write(row_num, 1, cic.card_print.card.name)
            worksheet.write(row_num, 2, cic.card_print.lang)
            worksheet.write(row_num, 3, cic.card_print.printed_name)
            worksheet.write(row_num, 4, cic.card_print.card.type)
            if cic.card_print.card.power is not None and cic.card_print.card.toughness is not None:
                worksheet.write(row_num, 5, '%s/%s' % (cic.card_print.card.power, cic.card_print.card.toughness))
            worksheet.write(row_num, 6, cic.card_print.card.mana_cost)
            worksheet.write(row_num, 7, cic.card_print.set_edition.name)
            worksheet.write(row_num, 8, cic.note)
            worksheet.write(row_num, 9, cic.collection.name)

            row_num += 1

        worksheet.autofilter(0, 0, row_num, 7)
        worksheet.freeze_panes(1, 0)
        worksheet.set_column(1, 1, width=25)
        worksheet.set_column(2, 2, width=7)
        worksheet.set_column(3, 4, width=25)
        worksheet.set_column(6, 6, width=25)
        worksheet.set_column(7, 9, width=35)

        workbook.close()
        output.seek(0)

        filename = 'magic-collection-export.xlsx'
        response = HttpResponse(
            output,
            content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        )
        response['Content-Disposition'] = 'attachment; filename=%s' % filename

    return response


def move_single_card_to_collection(card: CardInCollection, count: int, collection: Collection):
    if card.collection.user_id != collection.user_id:
        raise Exception('Cannot move across users')

    if count < 1:
        return
    if count > card.count:
        count = card.count

    # update / delete original card
    if count < card.count:
        card.count -= count
        card.save()
    else:
        card.delete()

    # is there same card already?
    existing_card = CardInCollection.objects.filter(
        card_print=card.card_print,
        collection=collection,
        foil=card.foil,
    ).first()  # type: CardInCollection

    if existing_card is not None:
        existing_card.count += count

        if card.note:
            if existing_card.note:
                existing_card.note += "; "
            existing_card.note += card.note

        existing_card.save()
        return

    new_card = CardInCollection(
        note=card.note,
        card_print=card.card_print,
        collection=collection,
        count=count,
        foil=card.foil,
    )
    new_card.save()


@login_required
@require_POST
def delete_card_in_collection(request):
    card_in_collection = get_object_or_404(CardInCollection, collection__user=request.user, id=request.POST['id'])

    card_in_collection.delete()
    messages.add_message(request, messages.SUCCESS, 'Card deleted.')

    return redirect(request.POST['next'])


@login_required
@require_POST
def add_to_collection(request):
    count = int(request.POST['cardCount'])
    collection_id = int(request.POST['collectionId'])
    card_print_scryfall_id = request.POST['cardScryfallId']
    foil = request.POST['foil'] == 'true'
    note = request.POST['note']

    collection = Collection.objects.filter(user=request.user, id=collection_id).get()

    card_print = get_or_create_card_print(card_print_scryfall_id)

    if count < 1:
        raise Exception('At least one card')

    # is there same card already?
    existing_card = CardInCollection.objects.filter(
        card_print=card_print,
        collection=collection,
        foil=foil,
    ).first()  # type: CardInCollection

    if existing_card is not None:
        existing_card.count += count

        if note:
            if existing_card.note:
                existing_card.note += "; "
            existing_card.note += note

        existing_card.save()

    else:
        CardInCollection(
            card_print=card_print,
            collection=collection,
            count=count,
            foil=foil,
            note=note,
        ).save()

    return JsonResponse({})


def qs_cards_to_result(qs):
    result = []
    for cic in qs:
        db_card_print = cic.card_print
        db_card = db_card_print.card
        db_set = db_card_print.set_edition
        db_collection = cic.collection

        result.append({
            'card_print': {
                'released_at': db_card_print.released_at,
                'image_uri_normal': db_card_print.image_uri_normal,
                'image_uri_small': db_card_print.image_uri_small,
                'lang': db_card_print.lang,
                'printed_name': db_card_print.printed_name,
                'scryfall_uri': db_card_print.scryfall_uri,
            },
            'card': {
                'id': db_card.id,
                'name': db_card.name,
                'mana_cost': db_card.mana_cost,
                'cmc': db_card.cmc,
                'type': db_card.type,
                'power': db_card.power,
                'toughness': db_card.toughness,
            },
            'collection': {
                'id': db_collection.id,
                'name': db_collection.name,
                'type': db_collection.type,
                'url': reverse('tgc:user_collection_detail', kwargs={
                    'user_id': db_collection.user.id,
                    'public_key': db_collection.user.userprofile.public_link_key,
                    'collection_id': db_collection.id,
                }),
            },
            'set': {
                'name': db_set.name,
                'code': db_set.code,
                'icon_svg_uri': db_set.icon_svg_uri,
                'released_at': db_set.released_at,
            },
            'count': cic.count,
            'foil': cic.foil,
            'note': cic.note,
            'id': cic.id,
        })

    return result


@require_GET
def get_user_cards(request, user_id: int, public_key: str):
    user = get_object_or_404(User, id=user_id, userprofile__public_link_key=public_key)

    qs = CardInCollection.objects.filter(collection__user=user) \
        .select_related('collection', 'card_print', 'card_print__card', 'card_print__set_edition',
                        'collection__user__userprofile') \
        .order_by('card_print__card__name').all()

    return JsonResponse({
        'cards': qs_cards_to_result(qs)
    })


@require_GET
def get_collection_cards(request, user_id: int, public_key: str, collection_id: int):
    user = get_object_or_404(User, id=user_id, userprofile__public_link_key=public_key)

    qs = CardInCollection.objects.filter(collection_id=collection_id, collection__user=user) \
        .select_related('collection', 'card_print', 'card_print__card', 'card_print__set_edition',
                        'collection__user__userprofile') \
        .order_by('card_print__card__name').all()

    return JsonResponse({
        'cards': qs_cards_to_result(qs)
    })
