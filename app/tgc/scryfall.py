import json
import urllib.parse

import requests

from tgc.models import Card, SetEdition, CardPrint


def get_or_create_card(oracle_id: str) -> Card:
    card = Card.objects.filter(oracle_id=oracle_id).first()

    if card is None:
        url = 'https://api.scryfall.com/cards/search?' + urllib.parse.urlencode({
            'q': 'oracleid=%s prefer:oldest' % oracle_id
        })
        response = requests.get(url)
        if not response.ok:
            raise Exception('Card "oracleid=%s" not found' % oracle_id)

        card_json = response.json()['data'][0]

        if 'mana_cost' in card_json:
            mana_cost = card_json['mana_cost']
        else:
            mana_cost = ' // '.join(face['mana_cost'] for face in card_json['card_faces'] if face['mana_cost'] != '')

        card = Card(
            name=card_json['name'],
            oracle_id=card_json['oracle_id'],
            mana_cost=mana_cost,
            cmc=card_json['cmc'],
            type=card_json['type_line'],
            card_json=json.dumps(card_json),
        )
        card.save()

    return card


def get_or_create_set(set_id: str) -> SetEdition:
    set_edition = SetEdition.objects.filter(scryfall_id=set_id).first()
    if set_edition is None:
        response = requests.get('https://api.scryfall.com/sets/%s' % set_id)
        if not response.ok:
            raise Exception('Set "id=%s" not found' % set_id)

        set_json = response.json()

        set_edition = SetEdition(
            scryfall_id=set_json['id'],
            code=set_json['code'],
            name=set_json['name'],
            icon_svg_uri=set_json['icon_svg_uri'],
        )
        set_edition.save()

    return set_edition


def get_or_create_card_print(card_print_scryfall_id: str) -> CardPrint:
    card_print = CardPrint.objects.filter(scryfall_id=card_print_scryfall_id).first()

    if card_print is None:
        # card print is missing, let's go create it
        response = requests.get('https://api.scryfall.com/cards/%s' % card_print_scryfall_id)
        if not response.ok:
            raise Exception('Card "scryfall_id=%s" not found' % card_print_scryfall_id)

        card_print_json = response.json()

        card = get_or_create_card(card_print_json['oracle_id'])
        set_edition = get_or_create_set(card_print_json['set_id'])

        if 'image_uris' in card_print_json:
            image_json = card_print_json['image_uris']
        elif 'card_faces' in card_print_json:
            image_json = card_print_json['card_faces'][0]['image_uris']
        else:
            image_json = {}

        card_print = CardPrint(
            set_edition=set_edition,
            card=card,
            lang=card_print_json['lang'],
            released_at=card_print_json['released_at'],
            scryfall_id=card_print_scryfall_id,
            card_json=json.dumps(card_print_json),
            image_uri_large=image_json.get('large'),
            image_uri_normal=image_json.get('normal'),
            image_uri_small=image_json.get('small'),
            image_uri_art_crop=image_json.get('art_crop'),
            printed_name=card_print_json.get('printed_name'),
        )
        card_print.save()

    return card_print
