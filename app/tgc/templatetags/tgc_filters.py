from typing import Optional

from django import template
from django.utils.safestring import mark_safe

from tgc.models import Collection

register = template.Library()


@register.filter
def collection_type_plural(collection: Optional[Collection]):
    if collection is None:
        return ''

    if collection.type == 'binder':
        return mark_safe('Binders <i class="bi bi-journals"></i>')

    if collection.type == 'deck':
        return mark_safe('Decks <i class="bi bi-box-seam"></i>')

    return collection.type


@register.filter
def collection_type_plural_no_icon(collection: Optional[Collection]):
    if collection is None:
        return ''

    if collection.type == 'binder':
        return 'Binders'

    if collection.type == 'deck':
        return 'Decks'

    return collection.type
