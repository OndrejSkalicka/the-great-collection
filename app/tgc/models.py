import random
import string

from django.conf import settings
from django.contrib.auth.models import User
from django.db import models
from django.db.models.functions import Lower
from django.db.models.signals import post_save
from django.dispatch import receiver


COLLECTION_TYPE_CHOICES = (
    ('binder', 'Binder'),
    ('deck', 'Deck'),
)


class SetEdition(models.Model):
    """
    A single MTG set.

    User agnostic.
    """
    scryfall_id = models.UUIDField(
        unique=True,
    )

    code = models.CharField(
        max_length=128,
    )
    name = models.CharField(
        max_length=128,
    )
    released_at = models.CharField(
        max_length=10,
    )

    icon_svg_uri = models.URLField()


class Card(models.Model):
    """
    A single card, without any set/quality/quantity/language specification.

    Eg. a card can be 'Swords to Plowshares', but not an 'Ice Age NM Swords to Plowshares'.

    It's effectively a another of grouping of cards. If you want to know, how many Llanowar Elves you have, you go to
    this class. Then you ask in what binders those are.

    Note that Cards are actually user independent. They define only what cards exist -- then it's up to user to say
    what does he have in his collection.
    """

    name = models.CharField(
        max_length=128,
    )

    oracle_id = models.UUIDField()

    mana_cost = models.CharField(
        max_length=128
    )

    cmc = models.FloatField()

    type = models.CharField(
        max_length=128,
    )

    power = models.CharField(
        max_length=4,
        null=True,
    )

    toughness = models.CharField(
        max_length=4,
        null=True,
    )

    card_json = models.TextField()


class CardPrint(models.Model):
    """
    A concrete card print, with a set definition, possible language (if implemented).

    Pretty close to Scryfalls' `card` object.

    Does not hold quantity or quality (or ownership) information.

    User agnostic.
    """

    set_edition = models.ForeignKey(
        SetEdition,
        on_delete=models.RESTRICT,
    )

    card = models.ForeignKey(
        Card,
        on_delete=models.RESTRICT,
    )

    printed_name = models.CharField(
        max_length=128,
        null=True,
    )

    lang = models.CharField(
        max_length=32,
        null=True,
    )

    released_at = models.CharField(
        max_length=10,
    )

    scryfall_id = models.UUIDField(
        unique=True
    )

    card_json = models.TextField()

    image_uri_large = models.URLField(
        null=True,
    )
    image_uri_normal = models.URLField(
        null=True,
    )
    image_uri_small = models.URLField(
        null=True,
    )
    image_uri_art_crop = models.URLField(
        null=True,
    )
    scryfall_uri = models.URLField(
        null=True,
    )


class Collection(models.Model):
    """
    Collection is binder, deck, an arbitrary grouping container for cards.
    """

    name = models.CharField(
        max_length=128,
    )

    note = models.CharField(
        max_length=128,
        default='',
    )

    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
    )

    type = models.CharField(
        max_length=16,
        choices=COLLECTION_TYPE_CHOICES,
        default=COLLECTION_TYPE_CHOICES[0][0]
    )

    def __str__(self):
        return 'Collection %s' % self.name

    class Meta:
        ordering = ['type', Lower('name'), ]


class CardInCollection(models.Model):
    created_at = models.DateTimeField(
        auto_now_add=True,
    )

    updated_at = models.DateTimeField(
        auto_now=True,
    )

    note = models.CharField(
        max_length=128,
        default='',
    )

    card_print = models.ForeignKey(
        CardPrint,
        on_delete=models.RESTRICT,
    )

    collection = models.ForeignKey(
        Collection,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
    )

    count = models.PositiveSmallIntegerField()

    foil = models.BooleanField(
        default=False,
    )


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    public_link_key = models.CharField(
        max_length=8,
    )

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if not self.public_link_key:
            self.public_link_key = ''.join(random.choices(string.ascii_lowercase + string.digits, k=8))

        super().save(force_insert, force_update, using, update_fields)


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        UserProfile.objects.get_or_create(user=instance)
