from django.urls import path

from . import views

app_name = 'tgc'
urlpatterns = [
    path('', views.index, name='index'),

    path('users/<int:user_id>/<str:public_key>/', views.user_home, name='user_home'),
    path('users/<int:user_id>/<str:public_key>/all-cards/', views.user_all_cards, name='user_all_cards'),
    path('users/<int:user_id>/<str:public_key>/collections/', views.user_collections, name='user_collections'),
    path('users/<int:user_id>/<str:public_key>/collections/<int:collection_id>/', views.user_collection_detail,
         name='user_collection_detail'),

    path('add-cards/', views.add_cards, name='add_cards'),
    path('add-cards/collection/<int:collection_id>', views.add_cards_to_collection, name='add_cards_to_collection'),

    path('cards/<int:user_id>/<str:public_key>', views.legacy_redirect_cards),

    path('api/collections/create/', views.create_collection, name='create_collection'),
    path('api/collections/edit/', views.edit_collection, name='edit_collection'),
    path('api/collections/delete/', views.delete_collection, name='delete_collection'),
    path('api/collections/delete-card', views.delete_card_in_collection, name='delete_card_in_collection'),
    path('api/move-from-collection', views.move_cards_from_to_collection, name='move_cards_from_to_collection'),
    path('api/move-card', views.move_card_to_collection, name='move_card_to_collection'),
    path('api/edit-card', views.edit_card, name='edit_card'),

    # TODO export just one collection + my_cards update
    path('api/export-cards-xlsx/<int:user_id>/<str:public_key>', views.export_cards_xlsx, name='export_cards_xlsx'),

    path('rest/add-to-collection/', views.add_to_collection, name='rest_add_to_collection'),

    path('rest/get-user-cards/<int:user_id>/<str:public_key>/', views.get_user_cards, name='rest_get_user_cards'),
    path('rest/get-collection-cards/<int:user_id>/<str:public_key>/<int:collection_id>/', views.get_collection_cards,
         name='rest_get_collection_cards'),
]
