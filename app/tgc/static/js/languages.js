const definedLanguages = {
    'en': {
        title: 'English',
        flag: 'flag-icon-gb',
    },

    'es': {
        title: 'Spanish',
        flag: 'flag-icon-es',
    },

    'fr': {
        title: 'French',
        flag: 'flag-icon-fr',
    },

    'de': {
        title: 'German',
        flag: 'flag-icon-de',
    },

    'it': {
        title: 'Italian',
        flag: 'flag-icon-it',
    },

    'pt': {
        title: 'Portuguese',
        flag: 'flag-icon-pt',
    },

    'ja': {
        title: 'Japanese',
        flag: 'flag-icon-jp',
    },

    'ko': {
        title: 'Korean',
        flag: 'flag-icon-kr',
    },

    'ru': {
        title: 'Russian',
        flag: 'flag-icon-russia',
    },

    'zhs': {
        title: 'Simplified Chinese',
        flag: 'flag-icon-cn',
    },

    'zht': {
        title: 'Traditional Chinese',
        flag: 'flag-icon-tw', // using Taiwan here, same as KMK
    },

    'he': {
        title: 'Hebrew',
        flag: 'flag-icon-il',
    },

    'la': {
        title: 'Latin',
        flag: null,
    },

    'grc': {
        title: 'Ancient Greek',
        flag: 'flag-icon-gr',
    },

    'ar': {
        title: 'Arabic',
        flag: 'flag-icon-sa',
    },

    'sa': {
        title: 'Sanskrit',
        flag: null,
    },

    'ph': {
        title: 'Phyrexian',
        flag: null,
    },
}

function flagElementWithText(code, title = true) {
    let dLang = definedLanguages[code];

    if (title) {
        if (dLang['flag'] === null) {
            return $('<span>')
                .append(dLang['title'])
                .attr('title', dLang['title']);
        }
        return $('<span>')
            .append(
                $('<span class="flag-icon">')
                    .addClass(dLang['flag'])
            )
            .append(' ' + dLang['title'])
            .attr('title', dLang['title']);
    }


    if (dLang['flag'] === null) {
        return $('<span>').attr('title', dLang['title']);
    }
    return $('<span>')
        .append(
            $('<span class="flag-icon">')
                .addClass(dLang['flag'])
        )
        .attr('title', dLang['title']);
}