$(function () {
    $('.local-stored')
        .change(function () {
            localStorage.setItem('add-card-' + $(this).attr('id'), $(this).val());
            console.log('stored');
        })
        .each(function () {
            if ($(this).data('local-stored-overwrite')) {
                $(this)
                    .val($(this).data('local-stored-overwrite'))
                    .change();

                return;
            }

            let val = localStorage.getItem('add-card-' + $(this).attr('id'));
            if (val !== null) {
                $(this).val(val);
            }
        });
});
