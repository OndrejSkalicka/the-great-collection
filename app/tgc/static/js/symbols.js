class SymbolTranslator {
    constructor() {
        this.symbolsSvg = {};
    }

    init(callback) {
        let self = this;

        $.get('https://api.scryfall.com/symbology',
            function (result) {
                _.forEach(result['data'], function (symbol) {
                    self.symbolsSvg[symbol['symbol']] = symbol['svg_uri'];
                });

                if (callback) {
                    callback();
                }
            });
    }

    costToSvg(cost, target) {
        let self = this;

        _.forEach([...cost.matchAll(/{[^}]+}|[^{]+/g)], function (m) {
            if (m in self.symbolsSvg) {
                target.append(
                    $('<img>')
                        .attr('src', self.symbolsSvg[m])
                        .attr('alt', m)
                        .addClass('svg-text')
                );
            } else {
                target.append(m);
            }
        });
    }
}
