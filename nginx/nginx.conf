upstream the_great_collection {
    server web:8000;
}

server {
    listen 80 default_server;
    server_name _; # This is just an invalid value which will never trigger on a real hostname.

    server_name_in_redirect off;

    root  /var/www/default/htdocs;
}

server {
    listen 80;

    server_name magic-collection.eu localhost 62.245.90.18;

    location /.well-known/acme-challenge/ {
        root /var/www/certbot;
    }

    location / {
        return 301 https://$host$request_uri;
    }
}

server {
    listen 443 ssl;
    server_name magic-collection.eu localhost 62.245.90.18;

    ssl_certificate /etc/letsencrypt/live/magic-collection.eu/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/magic-collection.eu/privkey.pem;

    gzip on;
    gzip_types      text/plain application/xml application/json text/html;
    gzip_proxied    no-cache no-store private expired auth;
    gzip_min_length 10240;

    location / {
        proxy_pass http://the_great_collection;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $host;
        proxy_redirect off;
    }

    location /static/ {
        alias /home/app/web/staticfiles/;
    }
}
