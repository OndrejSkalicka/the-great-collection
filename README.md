# The Great Collection

## Ideas / requirements

- import from KMK order (add to "arriving via post")
- different sortings (by creature/sorc/inst/ench/art/land aka "like a deck"; color, CMC, etc). But nice to have as decks management would be a bit more complicated here. See https://www.moxfield.com/decks/na--5Q3Mq0qB7HmPxZGnjg for an example.
- xlsx import

## TODOs

- user page management (actually, pretty nicely done here https://gymbeam.cz/customer/account/ )
- caching mana cost symbols
- get-icons -- possibly request from TGC server, cache there
- inline edit of a card (no-refresh)
- mailgun
- add foil overlay for cards that a) are foil and b) their cardprint does have both foil=true and nonfoil=true. Use this overlay https://www.seekpng.com/idown/u2w7w7w7w7a9e6e6_permalink-mtg-card-foil-overlay/ / https://www.seekpng.com/ipng/u2w7w7w7w7a9e6e6_permalink-mtg-card-foil-overlay/ (note that this is tricky, interacts poorly with popups etc)

## Nice scripts

```python
import os
import django

django.setup()

from tgc.models import CardInCollection
import json

prices = {}
for cic in CardInCollection.objects.filter(collection__user_id=1).select_related('collection', 'card_print',
                                                                     'card_print__card',
                                                                     'card_print__set_edition').all():
    if json.loads(cic.card_print.card_json)['prices']['eur'] is None:
        continue
    price = float(json.loads(cic.card_print.card_json)['prices']['eur'])
    if cic.collection.name not in prices:
        prices[cic.collection.name] = {
            'cost': 0.0,
            'count': 0
        }
    prices[cic.collection.name]['cost'] += price * cic.count
    prices[cic.collection.name]['count'] += cic.count

prices_sorted = sorted(list((x[0], int(x[1]['cost'])) for x in prices.items()), key=lambda x: -x[1])
# prices_sorted = sorted(list((x[0], int(x[1]['cost'])) for x in prices.items()), key=lambda x: -x[1]['cost'])

for name, total_cost in prices_sorted:
    print('%-40s: % 5d€ (%dx)' % (name, total_cost, prices[name]['count']))

print('Total %s€ (%dx)' % (int(sum(x['cost'] for x in prices.values())), sum(x['count'] for x in prices.values())))
print('Total _weight_: %.2fkg' % (sum(x['count'] for x in prices.values()) * 1.814 / 1000.0))

```
