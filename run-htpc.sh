#!/bin/bash

git pull

echo "Backing up DB"

docker-compose exec db-backup /backup.sh deploy_backup

echo "DB backed up, rebuilding images and redeploying."

docker-compose build && docker-compose down && docker-compose up -d && docker ps
